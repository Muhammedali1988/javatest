/*
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.util.ArrayList;


public class ChartsPlot extends Application {

    private ArrayList<Double> ma;
    private ArrayList<Double> data;
    private int window = 0;
    private String[] args;


    ChartsPlot(String[] args, ArrayList<Double> ma, ArrayList<Double> data, int window){
        this.args = args;
        this.ma = ma;
        this.data = data;
        this.window = window / 2;
    }

    @Override
    public void start(Stage stage) {

        stage.setTitle("Line Chart Sample");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Days");
        final LineChart<String,Number> lineChart = new LineChart<String, Number>(xAxis,yAxis);

        lineChart.setTitle("Dollar for 212 days");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Dollar");

        for(int i = 1; i < this.data.size(); i++){
            series1.getData().add(new XYChart.Data(i, this.data.get(i)));
        }



        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Simple Moving Average");
        for(Integer j = 0; j < this.ma.size(); j++){
            if(j < this.window) {
                series2.getData().add(new XYChart.Data("", null));
            }
            else{
                series2.getData().add(new XYChart.Data(j, this.ma.get(j)));

            }
        }
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series1, series2);

        stage.setScene(scene);
        stage.show();
    }

    public void GetPlot() {
        launch(this.args);
    }
}
*/