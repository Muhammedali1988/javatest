FROM openjdk:8-jdk

RUN apt-get update && \
    apt-get upgrade && \
    apt-get install -y maven 

RUN mvn --version
