
public class Poly {
    double[] arr;

    Poly(String[] argv){
        try {
            arr = new double[argv.length - 1];
            for (int i = 1; i < argv.length; i++) {
                double temp = Double.parseDouble(argv[i]);
                arr[i - 1] = temp;
            }
        }catch (Exception e){
            System.out.println("Косяк ввода: \n" + e.getMessage());
        }

    }

    public double get_poly(){
        double add = 0.0;
            for(int i = 0; i < arr.length; i++)
                add += 1 / arr[i] * 3;
        return add;
    }
}
