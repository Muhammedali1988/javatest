import java.io.IOException;

public class Calc {
    private double fval = 2.0;
    private double sval = 0.0;
    private String c = "+";

    Calc(double fval, double sval, String c) {
        this.fval = fval;
        this.sval = sval;
        this.c = c;
    }

    Calc() {
    }

    private double addition() {
        return this.fval + this.sval;
    }

    private double devision() {
        if (this.sval != 0) {
            return this.fval / this.sval;
        } else {
            System.out.println("знаменатель не может быть равен 0!!!");
            return 0;
        }
    }

    private double multiplication() {
        return this.fval * this.sval;
    }

    private double subtraction() {
        return this.fval - this.sval;
    }

    public double GetResult() {
        if (this.c.equals("+")) {
            return addition();
        } else if (this.c.equals("-")) {
            return subtraction();
        } else if (this.c.equals("*")) {
            return multiplication();
        } else if (this.c.equals("/")) {
            return devision();
        } else {
            System.out.println("ERROR");
            return 0;
        }

    }
}

