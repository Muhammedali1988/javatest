import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            System.out.println("Первое и второе значения целые или дробные числа!!!");
            Scanner in = new Scanner(System.in);
            System.out.print("Enter first value: ");
            double a = in.nextDouble();
            System.out.print("Enter second value: ");
            double b = in.nextDouble();
            System.out.print("Enter +,-,*,/: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String c = reader.readLine();
            Calc test = new Calc(a, b, c);

            System.out.println(test.GetResult());


        } catch (Exception e) {
            System.out.println("Косяк ввода: " + e.getMessage());
        }
    }
}
