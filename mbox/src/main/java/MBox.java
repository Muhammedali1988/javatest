import sun.awt.X11.XSystemTrayPeer;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.locks.Condition;

/*
1 Probability
2 condofemce
*/
public class MBox {
    private ArrayList<String> cMail;
    private ArrayList<String> OnlyMail;
    public Map<String, Integer> CalcMails = new HashMap<String, Integer>();
    MBox(ArrayList<String> str){
        this.cMail = str;
    }

    private  ArrayList<String> GetSplit(ArrayList<String> arr){
        ArrayList<String> splitMail = new ArrayList<String>();
        for(int i = 0; i < arr.size(); i++) {
            String[] tmp;
            String a;
            a = arr.get(i);
            tmp = a.split(" ");
            for(int j =0; j < tmp.length; j++){
                splitMail.add(tmp[j]);
            }
        }
        return splitMail;
    }

    public ArrayList<String> GetEmail(){
        ArrayList<String> spl; // строки разделённые пробедлами
        spl = GetSplit(this.cMail);
        ArrayList<String> mail = new ArrayList<String>();
        String sh = "From";
        String site = "Site";
        for(int i = 0; i < spl.size(); i++){
            if(sh.equals(spl.get(i)))
                if(!site.equals(spl.get(i+1)))
                    mail.add(spl.get(i+1));

        }
        return mail;
    }
    public Map<String, Integer> GetCalcMails(){
        ArrayList<String> mail = GetEmail();
        Set<String> unique = new HashSet<String>(mail);
        for (String key : unique) {
            this.CalcMails.put(key, Collections.frequency(mail, key));
        }

        return this.CalcMails;
        }

    private ArrayList<Double> CalcConfi(){
        ArrayList<String> spl; // строки разделённые пробедлами
        spl = GetSplit(this.cMail);
        ArrayList<Double> results = new ArrayList<Double>();
        String sh = "X-DSPAM-Confidence:";
        for(int i = 0; i < spl.size(); i++){
            if(sh.equals(spl.get(i))){
                String tmp = spl.get(i+1);
                results.add(Double.parseDouble(tmp));
            }
        }
        return results;
    }

    public double AValue(){
        ArrayList<Double> confi = CalcConfi();
        double result = 0.0;
        for(int i = 0; i < confi.size(); i++)
            result += confi.get(i);

        return result/confi.size();
    }


    public void getSpamer() {
        ArrayList<String> mails = GetEmail();
        ArrayList<Double> confidence = CalcConfi();

        Collections.sort(mails);

        Collections.sort(confidence);

        for(int i = 0; i < confidence.size()-1; i++)
            if (confidence.get(i) > 0.97)
                if (!mails.get(i).equals(mails.get(i + 1)))
                    System.out.println(mails.get(i) + " спамер с вероятностью:" + confidence.get(i));
        // УДАЛИ ЭТО Н
        System.out.println(mails.get(mails.size()) + " спамер с вероятностью:" + confidence.get(mails.size()));
    }
}

